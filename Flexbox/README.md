# Des mises en page fluides avec Flexbox

Le module Flexbox est, comme son nom le laisse deviner, un modèle de "boîtes flexibles" pour pages Web. Ce système permet une plus grande souplesse de mise en forme du contenu par le biais d'une hiérarchisation du contenu (système d'éléments parent-enfant). Ce tutoriel présente les bases du fonctionnement de Flexbox mais le potentiel de l'outil est bien plus vaste.

## Quelques ressources

- L'article Flexbox sur MDN : https://developer.mozilla.org/fr/docs/Learn/CSS/CSS_layout/Flexbox

- A Complete Guide to Flexbox : https://css-tricks.com/snippets/css/a-guide-to-flexbox/
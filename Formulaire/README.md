# Créer un formulaire en HTML 5 et CSS 3

Les formulaires sont le moyen de communication entre un utilisateur et un site le plus largement répandu sur le Web : on s'en sert pour contacter quelqu'un, passer une commande, remplir nos documents administratifs...

Ce tutoriel a pour but de comprendre les possibilités qu'offre l'HTML 5 pour structurer un formulaire et permettre une interprétation du code plus précise par le navigateur, grâce à la notion d'HTML sémantique

## Quelques sources

- Les éléments HTML sémantiques : https://www.w3schools.com/html/html5_semantic_elements.asp

- JotForm, un générateur de formulaires en ligne : https://www.jotform.com/fr/
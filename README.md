# Les tutoriels du magazine "Digital Creative" n°11

Pour plus d'explications sur le contenu de chaque tutoriel, n'hésitez pas à acheter ce numéro :

![image info](./images/couverture_digitalcreative11.jpg "Couverture du n°11 du magazine Digital Creative (image tirée du site journaux.fr : https://www.journaux.fr/digital-creative_generaliste_informatique_238437.html)")


## Où trouver le magazine ?

Chez votre marchand de journaux le plus proche, ou en commande à cette adresse : https://www.journaux.fr/digital-creative_generaliste_informatique_238437.html

## Pourquoi ce repo ?

En cherchant des magazines techniques chez mon marchand de journaux, je suis tombé sur celui-ci, que j'ai trouvé très clair et riche en contenu. Mais lire du code dans une revue papier sans le voir mis en application, c'est toujours un peu frustrant, donc j'ai décidé de reproduire les tutoriels un par un, aussi bien pour transmettre ce contenu à d'autres que pour retravailler mes compétences de base dans le web design.

Cerise sur le gâteau : chaque tutoriel contient plusieurs autres sources pour approfondir chaque concept, ce qui en fait une bonne source de liens à sauvegarder sur votre navigateur au besoin.

## La construction du repo

- Chaque tutoriel est rangé dans un dossier spécifique, contenant des fichiers à manipuler, ainsi qu'un README résumant sommairement le concept présenté dans le tuto

- Les tutoriels sont divisés en plusieurs étapes (notés "Step" dans les fichiers), qui modifient de différentes façons le rendu de la page, à vous donc d'essayer les différentes étapes en vous aidant du magazine, pour comprendre les principes qui sont mis en application à chaque "step".

## Quelques ressources générales

Les sites ci-dessous sont parmi les plus utilisés par les développeurs en recherche d'informations. Bien sûr, Internet regorge d'une quantité incroyable de sources et de contenu mais il faut bien commencer quelque part !

- Mozilla Developer Network (ou MDN) : https://developer.mozilla.org/fr/

- Stack Overflow : https://stackoverflow.com/

- W3Schools : https://www.w3schools.com/

- Youtube : https://www.youtube.com/

- Can I Use : https://caniuse.com/
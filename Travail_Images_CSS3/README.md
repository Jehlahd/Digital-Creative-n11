# Travailler les images grâce au CSS 3

Dans ce tutoriel, vous allez approfondir votre connaissance du CSS par la découverte de nouvelles propriétés et vous allez découvrir comment peuvent être intégrer des images sur un site Web.

Pour compléter un peu le tutoriel, j'ai utilisé des images provenant de plusieurs sources différentes : certaines sont au même niveau que le fichier HTML, d'autres sont dans un dossier spécifique et quelques unes viennent directement d'une URL externe (ça vous permettra de découvrir un peu le principe des chemins d'accès aux fichiers)

## Quelques ressources

- Lorem Picsum : https://picsum.photos/

- L'outil "Clippy" de Bennett Feely : https://bennettfeely.com/clippy/
(Basé sur la prorpiété expérimentale "clip-path", encore mal supportée par certains navigateurs, il y a un article à son sujet sur MDN)
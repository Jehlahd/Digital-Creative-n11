# Créer des effets de survol en CSS 3

Le principe de survol, c'est simplement le fait de faire passer sa souris sur un élément d'une page. Or, malgré toute la banalité de cette action, il est possible de mettre en forme de façon poussée des réactions au survol, pour rendre l'affichage du site plus "interactif". Ce tutoriel se concentre donc sur le survol et la manipulation du CSS pour y arriver.

Attention, même si les effets de survol sont amusants, il est préférable de ne pas en abuser et de les utiliser pour guider l'utilisateur vers des parties importantes du site

## Quelques ressources

- ImageHover.css : https://imagehover.io/

- Original Hover Effects with CSS3 : https://tympanus.net/codrops/2011/11/02/original-hover-effects-with-css3/
